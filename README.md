mantis-to-gitlab
================

This script is meant to be used on a CSV export from a Mantis instance in
order to import the issues into GitLab. It aims to keep the issue numbers the
same, so that Mantis Issue #1000 is GitLab issue #1000. The target project
must not have any issues already open and while the script is running, no
issues may be opened (off-by-one will happen otherwise).

This software was originally written by
[kitware](https://gitlab.kitware.com/utils/mantis-to-gitlab) and is being
updated to work with the recent versions of gitlab (11/2018) and to copy more
data than the basic things it did. More details to follow.
